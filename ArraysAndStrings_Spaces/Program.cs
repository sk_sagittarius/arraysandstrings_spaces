﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysAndStrings_Spaces
{
    class Program
    {
        static void Main(string[] args)
        {
            char key = Console.ReadKey().KeyChar;
            int count = 0;
            List<string> list = new List<string>();
            for (; ; )
            {
                do
                {
                    key = Console.ReadKey().KeyChar;
                    if (key != 46)
                    {
                        list.Add(Char.ToString(key));
                    }
                    if (key == 32)
                    {
                        count++;
                    }
                } while (key != 46);
                break;
            }
            Console.WriteLine("\n" + count);
            Console.ReadLine();
            Console.ReadLine();
        }
    }
}
